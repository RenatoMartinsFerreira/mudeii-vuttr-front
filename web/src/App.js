import React, { useState } from 'react';
import { ToolComponent, SearchBarComponent } from './components/';
import './css/App.css';
import './css/global.css';

function App() {
  const [tools, setDevs] = useState([
    { title: 'json-server', description: 'Get a full fake REST API with zero coding in less than 30 seconds (seriously)Get a full fake REST API with zero coding in less than 30 seconds (seriously)Get a full fake REST API with zero coding in less than 30 seconds (seriously)Get a full fake REST API with zero coding in less than 30 seconds (seriously)', tags: ['api', 'json'] },
    { title: 'Iconmoon', description: 'Ferramenta para transformar SVG em Fontes', tags: ['svg', 'font', 'react-native'] },
  ])

  return (
    <div id="App">
      <div>
        <header className="App-header">
          <h1> VUTTR </h1>
          <h2> Very Useful Tools to Remember </h2>
        </header>
        <main>
          <div className='settingsBar' >
            <div className='SearchBarContainer' >
              <SearchBarComponent />
              <input type="checkbox" name="onlyTags" />
              <label className='onlyTagsLabel' htmlFor='onlyTags' > Search in tags only </label>
            </div>

            <input className='addButton' type='button' value='ADD +' />

          </div>
          <ul>
            {tools.map(tool => (
              <div className='toolContainer' >
                <ToolComponent
                  className="toolContainer"
                   tool={tool}
                   />
              </div>
            ))}
          </ul>
        </main>
      </div>
    </div>
  );
}

export default App;
