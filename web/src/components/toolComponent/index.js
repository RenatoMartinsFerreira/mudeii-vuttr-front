import React from 'react';
import './toolComponent.css';

import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


class ToolComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleClick = (e) => {
    e.preventDefault();
    console.log('The link was clicked.');
  };
  render(tool) {
    return (

      <div className="toolComponent" >

        <div className='headerContainer' >
          <p className='toolTitle'> {this.props.tool.title} </p>
          <a className='removeButton' onClick={this.handleClick} >
            <p> Remove </p>
            <FontAwesomeIcon size="sm" icon={faTrash} style={{ marginLeft: 10 }} />

          </a>

        </div>

        <p className='toolDescription'> {this.props.tool.description} </p>

        {this.props.tool.tags.map(tag => (
          <strong className='toolTags'> #{tag} </strong> 
        ))}

      </div >
    );
  }
}

export { ToolComponent };
