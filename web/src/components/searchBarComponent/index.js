import React from 'react';
import './seacrhBarComponent.css';

import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function SearchBarComponent(onChange) {
  return (
    <div className="searchBarComponent">
      <FontAwesomeIcon size="sm" icon={faSearch} style={{ color: '#0089BB' }} />
      <input type='text' />
    </div>
  );
}

export { SearchBarComponent };
